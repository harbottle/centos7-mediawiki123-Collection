%global rel 1_23
%global checkout bf7c4b9

Name:           mediawiki123-Collection
Version:        1.7.0
Release:        1%{?dist}
Summary:        Organize personal selections of pages in a collection

License:        GPLv2
URL:            https://www.mediawiki.org/wiki/Extension:Collection
Source0:        https://extdist.wmflabs.org/dist/extensions/Collection-REL%{rel}-%{checkout}.tar.gz
BuildArch:      noarch

Requires:       mediawiki123

%description
The Collection extension allows a user to organize personal selections of
pages in a collection. Collections can be

  - edited and structured using chapters
  - persisted, loaded and shared
  - rendered as PDF (see Extension:PDF_Writer and Extension:Collection/OfflineContentGenerator TODO)
  - exported as ODF Text Document (see Extension:OpenDocument_Export)
  - exported as DocBook XML (see Extension:XML_Bridge)
  - ordered as a printed book at http://pediapress.com/
  - exported as ZIM file (see Extension:Collection/openZIM)
  - exported as ePub file (ebook)

%prep
%setup -q -n Collection
echo 'To complete installation of %{name}, add the following lines to LocalSettings.php:

    require_once("$IP/extensions/Collection/Collection.php");

for each MediaWiki instance you wish to install %{name} on.' > README.centos

%build

%install
mkdir -p %{buildroot}%{_datadir}/mediawiki123/extensions/Collection/
mkdir -p %{buildroot}%{_datadir}/mediawiki123/extensions/Collection/i18n/
mkdir -p %{buildroot}%{_datadir}/mediawiki123/extensions/Collection/images/
mkdir -p %{buildroot}%{_datadir}/mediawiki123/extensions/Collection/js/
mkdir -p %{buildroot}%{_datadir}/mediawiki123/extensions/Collection/OfflineContentGenerator/
install -cpm 644 %{_builddir}/%{buildsubdir}/*.php %{buildroot}%{_datadir}/mediawiki123/extensions/Collection/
install -cpm 644 %{_builddir}/%{buildsubdir}/i18n/*.json %{buildroot}%{_datadir}/mediawiki123/extensions/Collection/i18n/
install -cpm 644 %{_builddir}/%{buildsubdir}/images/*.png %{buildroot}%{_datadir}/mediawiki123/extensions/Collection/images/
install -cpm 644 %{_builddir}/%{buildsubdir}/js/*.{js,css} %{buildroot}%{_datadir}/mediawiki123/extensions/Collection/js/

%files
%doc README.centos COPYING README.rst README.txt version
%{_datadir}/mediawiki123/extensions/Collection

%changelog
* Tue May 16 2017 Richard Grainger <grainger@gmail.com> - 1.7.0-1
- Initial packaging
