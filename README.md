# centos7-mediawiki123-Collection
[![build status](https://gitlab.com/harbottle/centos7-mediawiki123-Collection/badges/master/build.svg)](https://gitlab.com/harbottle/centos7-mediawiki123-Collection/builds)

Build MediaWiki Collection extension RPM for CentOS 7 using GitLab CI.

List of RPM packages:
 - wildfly (application server)
 - wildfly-doc (documentation)
 - wildfly-oracle (Oracle JDBC driver.  Requires Oracle InstantClient)

You can
[browse and download](https://gitlab.com/harbottle/centos7-mediawiki123-Collection/builds/artifacts/master/browse?job=build)
the generated RPM packages from the latest successful CI build.
