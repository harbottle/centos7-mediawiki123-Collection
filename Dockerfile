FROM centos:latest
MAINTAINER harbottle
RUN yum -y install rpm-build
RUN yum -y install yum-utils
RUN yum -y install rpmdevtools
ADD SPECS/*.spec /root/
RUN yum-builddep -y /root/*.spec
